<?
if (isset($_POST['submit'])){
    $text = $_POST['taText'];
    $_COOKIE['txt'] = $text;
    $count_text = strlen($text);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task 1</title>
</head>
<body>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
    <textarea name="taText" id="taText" cols="30" rows="3"><?=$_COOKIE['txt']?></textarea>
    <input type="submit" name="submit" value="Submit">
</form>
<div class="countedSimbols">
    <p>Simbols in textarea: <?=$count_text?></p>
</div>
</body>
</html>